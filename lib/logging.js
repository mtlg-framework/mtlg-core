
var
  moduleSwitches = {},
  logSwitches = {},
  warnSwitches = {},
  deprecateSwitches = {};

/**
 * Turn on logging for the module specified by <i>id</i>.
 * @param {...string} ids The module ids for which logging shall be turned on.
 * @memberof MTLG#
 */
var startLog = (...ids) => {
  for(var id of ids) moduleSwitches[id] = true;
}
/**
 * Turn off logging for the module specified by <i>id</i>.
 * @param {...string} ids The module ids for which logging shall be turned off.
 * @memberof MTLG#
 */
var endLog = (...ids) => {
  for(var id of ids) moduleSwitches[id] = false;
}

// the following functions try to keep a low call stack

/**
 * Log a message from the module specified by <i>id</i>.
 * @param {string} id The module id associated with this log.
 * @param {...string} msg The messages to be printed out.
 * @memberof MTLG#
 */
var log = (id, ...msg) => {
  if(moduleSwitches[id]) console.log(...msg)
};
/**
 * Log a message from the module specified by <i>id</i>. This function checks whether <i>msgId</i> was already printed out previously. If yes, nothing is printed.
 * @param {string} id The module id associated with this log.
 * @param {string} msgId The message identifier, there will be only one print even if this method is called multiple times.
 * @param {...string} msg The messages to be printed out.
 * @memberof MTLG#
 */
var logOnce = (id, msgId, ...msg) => {
  if(!logSwitches[`${id}_${msgId}`]) {
    logSwitches[`${id}_${msgId}`] = true;
    if(moduleSwitches[id]) console.log(...msg);
  }
};
/**
 * Log a message from the module specified by <i>id</i> with warning level.
 * @param {string} id The module id associated with this log.
 * @param {...string} msg The messages to be printed out.
 * @memberof MTLG#
 */
var warn = (id, ...msg) => {
  console.warn(...msg);
}
/**
 * Log a message from the module specified by <i>id</i> with warning level. This function checks whether <i>msgId</i> was already printed out previously. If yes, nothing is printed.
 * @param {string} id The module id associated with this log.
 * @param {string} msgId The message identifier, there will be only one print even if this method is called multiple times.
 * @param {...string} msg The messages to be printed out.
 * @memberof MTLG#
 */
var warnOnce = (id, msgId, ...msg) => {
  if(!warnSwitches[`${id}_${msgId}`]) {
    warnSwitches[`${id}_${msgId}`] = true;
    console.warn(...msg);
  }
};
/**
 * Log a deprecation warning. The warning will be printed only once per unique <i>deprecatedUse</i>, even if it is called multiple times.
 * @param {string} deprecatedUse The function name or procedure to be deprecated.
 * @param {string} recommendedUse The recommended function name or procedure to use instead.
 * @memberof MTLG#
 */
var deprecate = (deprecatedUse, recommendedUse)  => {
  if (!deprecateSwitches[deprecatedUse]) {
    deprecateSwitches[deprecatedUse] = true;
    console.warn(`Deprecated use: '${deprecatedUse}', use '${recommendedUse}' instead`);
  }
}

Object.assign(MTLG, {
  startLog,
  endLog,
  log,
  logOnce,
  warn,
  warnOnce,
  deprecate
})
