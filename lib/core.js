/**
 * @Author: Thiemo Leonhardt <thiemo>
 * @Date:   2017-08-17T19:26:38+02:00
 * @Email:  leonhardt@cs.rwth-aachen.de
 * @Last modified by:   thiemo
 * @Last modified time: 2018-02-22T16:00:02+01:00
 */

/**
 * MTLG framework
 *
 * @namespace MTLG
 *
 **/

/*
 * exoprt module registration mechanism
 */
Object.assign(exports, { addModule });

// temporary workaround for missing createjs module support
require('tweenjs/lib/tweenjs-NEXT.js');
require('preloadjs/lib/preloadjs-NEXT.js');
require('soundjs/lib/soundjs-NEXT.js');
require('./easeljs_patch979');

/*
* obligatory core modules
*/
require('./logging');
require('./assetManager');
require('./langHandler');
require('./lifeCycleDemo');
require('./userLogin');

/*
 * required files
 */
require('./core.css');
require.context('../img');

/*
 * program variables (initialization further below)
 */
var stage,
  stageContainer,
  options,
  settings,
  modulsMTLG,
  players,
  moduleInit,
  moduleInfo,
  blockingModules,
  gameInit,
  backgroundStage,
  anonToken,
  scaleFactor;

/**
 * This function initializes the learning game.
 * It is called when the onload-event of body in index.html is fired.
 * The function loads all settings, creates a stage, initializes modules
 * and calls the game init function.
 * @function init
 * @memberof MTLG#
 */
var mtlg_init = function() {

  /*
   * Default-Options
   * language is set as follows:
   * 1. are languages defined in the browser?
   * 2. if yes, take the top language - end
   * 3. if no, take the user preferred language - end
   */
  options = options || {};
  defaults = {
    get hoehe() {
      MTLG.deprecate("options.hoehe", "options.height");
      return this.height;
    },
    set hoehe(v) {
      MTLG.deprecate("options.hoehe", "options.height");
      this.height = v;
    },
    get breite() {
      MTLG.deprecate("options.breite", "options.width");
      return this.width;
    },
    set breite(v) {
      MTLG.deprecate("options.breite", "options.width");
      this.width = v;
    },
    width: screen.width,
    height: screen.height,
    dd: {
      host: "http://kildin.informatik.rwth-aachen.de",
      port: 3125
    },
    zoll: 27,
    countdown: 180,
    fps: 60,
    playernumber: 4,
    LCDMbaseUrl: 'http://ostrov.informatik.rwth-aachen.de:5080/',
    mockLogin: false, //Skip login and use dummies instead
    mockNumber: 4, //Number of dummies created
    webgl: false
  };

  /*
   * use default values when no parameter are given
   */

  options = Object.assign(defaults, options);

  /*
   * Load Options and settings provided by window.mtlgClient
   */
  if(window.mtlgClient) {
    if(window.mtlgClient.gameSettings) loadSettings(window.mtlgClient.gameSettings);
    if(window.mtlgClient.coreConfig) loadOptions(window.mtlgClient.coreConfig);
    if(window.mtlgClient.deviceConfig) loadOptions(window.mtlgClient.deviceConfig);
  }

  /*
   * Set the language for the game:
   * First try to find language setting (set in device_manifest)
   * Next try to set the browser prefered language as game language.
   * Choose the first available language else.
   */
  if (!options.language || !MTLG.lang.setLanguage(options.language)) {
    MTLG.log("core", "Device manifest language could not be set: " + options.language);
    var browserLang = navigator.languages ? navigator.languages[0] : navigator.language;
    browserLang = browserLang.split("-")[0];
    if (!MTLG.lang.setLanguage(browserLang)) {
      MTLG.log("core", "Browser language " + browserLang + " not supported!");
      MTLG.lang.setLanguage(options.languages[0]);
    }
  }

  //adapt canvasObject to the requirements from options:
  var canvas = document.getElementById('canvasObject');

  //assign canvasObject
  if(options.webgl) {
    stage = new createjs.StageGL(canvas, {directDraw:false});
    stage.setClearColor("#001");
    // workaround for https://github.com/CreateJS/EaselJS/issues/978
    let dummy = new createjs.Shape();
    dummy.cache(0,0,1,1);
    stage.addChild(dummy);
    stage.update();
    stage.removeChild(dummy);
  } else {
    stage = new createjs.Stage(canvas);
  }
  stageContainer = new createjs.Container();
  stageContainer.setBounds(stage.getBounds());
  stage.addChild(stageContainer);

  //enable touch
  createjs.Touch.enable(stage);
  // deprecate global stage object
  Object.defineProperty(window, 'stage', {
    get: function() {
      MTLG.deprecate("global stage", "MTLG.getStage")
      return window.__stage
    },
    set: function(v) {
      MTLG.deprecate("global stage", "MTLG.getStage")
      window.__stage = v
    }
  });

  // add background canvas
  backgroundStage = new createjs.Stage(_addBackgroundCanvas());

  // scale stage to window size and listen to changes
  resize_canvas();
  document.body.onresize = resize_canvas;

  // block context menu on long touch
  document.addEventListener('contextmenu', (e) => { e.preventDefault(); return false })

  players = [];

  // Initialize modules
  moduleInit = moduleInit || []; //Make sure this works even if no module was registered
  moduleInfo = moduleInfo || []; //Make sure this works even if no module was registered

  blockingModules = {}; //Some modules have to be present before the game starts
  for (var i = 0; i < moduleInit.length; i++) {
    if (modulsMTLG && modulsMTLG.hasOwnProperty(moduleInfo[i]().name) && modulsMTLG[moduleInfo[i]().name] === 0) { // moduleInfo[i]().name === 'tabulaEvents'
      MTLG.log("core", 'Modul ' + moduleInfo[i]().name + ' not loaded, because it is blocked in manifest/framework.settings.js.');
    } else {
      try {
        var retValue = (moduleInit[i])(options, moduleCallback);
        if (retValue) { //Blocking modules return their handle
          blockingModules[retValue] = true;
        }
      } catch (err) {
        MTLG.warn("core", "Error calling init function: ");
        MTLG.warn("core", err);
        MTLG.warn("core", moduleInit[i]);
      }
    }
  }

  //Initialize game
  try {
    gameInit(options);
  } catch (err) {
    MTLG.warn("core", "Error calling game init function: ");
    MTLG.warn("core", err);
    MTLG.warn("core", gameInit);
  }

  // there are no blocking modules
  if (Object.keys(blockingModules).length == 0)
    allModulesDone();

  // expose MTLG and createjs to the global namespace for debugging in the web developer console
  window.MTLG = MTLG;
  window.window.createjs = createjs;
}

function _addBackgroundCanvas() {
  let newNode = stage.canvas.cloneNode(true);
  newNode.setAttribute('id', 'background');
  stage.canvas.parentNode.insertBefore(newNode, stage.canvas);
  return newNode;
}

/**
 * This function is called by blocking modules once they are done.
 * If all blocking modules are finished, this function calls allModulesDone.
 * @param moduleHandle the handle, as specified in the return value of the module init function
 * @memberof MTLG#
 */
function moduleCallback(moduleHandle) {
  if (blockingModules[moduleHandle]) {
    blockingModules[moduleHandle] = false;
  } else {
    MTLG.log("core", "Callback from unknown module: " + moduleHandle);
  }
  var modulesLeft = false;
  for (var property in blockingModules) {
    if (blockingModules.hasOwnProperty(property)) {
      if (blockingModules[property]) {
        modulesLeft = true;
        break;
      }
    }
  }
  if (!modulesLeft) {
    allModulesDone();
  }
}

/**
 * This function is called after all modules have been initialized.
 * It starts the game and stage update ticker.
 * @memberof MTLG#
 */
function allModulesDone() {
  MTLG.log("core", "All modules loaded");
  // start game
  createjs.Ticker.framerate = options.fps;
  createjs.Ticker.addEventListener('tick', stage);
  MTLG.lc.startGame(); //lc takes care of login etc.

};

/**
 * resize all canvas overlays, keeping aspect ratio
 * @function resize
 * @memberof MTLG#
 */
function resize_canvas() {
  var canvas = document.getElementsByTagName('canvas'),
    i;
  for (i = 0; i < canvas.length; i = i + 1) {
    canvas[i].height = window.innerHeight;
    canvas[i].width = window.innerWidth;
  }

  scaleFactor = MTLG.lc.resizeStage();

  // update GL viewport
  if(MTLG.getStage().isWebGL)
    MTLG.getStage().updateViewport(MTLG.getOptions().width*scaleFactor, MTLG.getOptions().height*scaleFactor);
}

/**
 * Add an initialization callback for a module.
 * This will be called in MTLG.init().
 * @param {function} initCallback - the init function of the new module
 * @param {function} info - a function that returns version, name and type information of the module
 * @memberof MTLG#
 */
function addModule(initCallback, info) {
  moduleInit = moduleInit || [];
  moduleInit.push(initCallback);
  moduleInfo = moduleInfo || [];
  info = info || function() {
    return {
      version: 'No version added.',
      name: 'No name added.',
      type: 'No type added.'
    }
  };
  moduleInfo.push(info);
}

/**
 * This function is used to register the game init function with the MTLG framework.
 * The callback is normally defined in <strong>dev/game.js</strong> .
 * @param {function} initCallback - init function of game
 * @memberof MTLG#
 */
function addGameInit(initCallback) {
  gameInit = initCallback;
}


/////////////////////////////////

/**
 * This function returns the stage object
 * @memberof MTLG#
 */
var getStage = function() {
  return stage;
};

/**
 * This function clears the stage by removing all children
 * @memberof MTLG#
 */
var clearStage = function() {
  stage.removeAllChildren();
  stageContainer.removeAllChildren();
  stage.addChild(stageContainer.set({
    scaleX: 1,
    scaleY: 1
  }));
};

/**
 * Load new options - overwrites old options where the same key is used
 * @param {object} newOptions - an object containing the new options
 * @memberof MTLG#
 */
var loadOptions = function(newOptions) {
  options = options || {};
  //Override local settings if existing
  for (var prop in newOptions) {
    options[prop] = newOptions[prop];
  }
};

/**
 * Load new settings - overwrites old settings where the same key is used
 * @param {object} newSettings - an object containing the new settings
 * @memberof MTLG#
 */
var loadSettings = function(newSettings) {
  settings = settings || {
    default: {},
    all: {}
  };
  //Override local settings if existing
  for (var prop in newSettings) {
    if (!settings[prop]) settings[prop] = newSettings[prop]
    for (var prop2 in newSettings[prop]) {
      settings[prop][prop2] = newSettings[prop][prop2];
    }
  }
};

/**
 * overwrites old module options
 * @param {object} newOptions - the new module options
 * @memberof MTLG#
 */
var loadModulsMTLG = function(newOptions) {
  modulsMTLG = modulsMTLG || {};
  //Override local settings if existing
  for (var prop in newOptions) {
    modulsMTLG[prop] = newOptions[prop];
  }
};

/**
 * This function returns the number of players
 * @memberof MTLG#
 */
var getPlayerNumber = function() {
  return players.length;
};

/**
 * This function adds a player and returns its index
 * @param newPlayer - the player to be added
 * @memberof MTLG#
 */
var addPlayer = function(newPlayer) {
  players.push(newPlayer);
  return players.length -1;
};

/**
 * This function updates a player
 * @param index - the players index
 * @param updatedPlayer - the updated player
 * @memberof MTLG#
 */
var updatePlayer = function(index, updatedPlayer) {
  players[index] = updatedPlayer;
};

/**
 * This function returns a specific player
 * @param {number} index - the player's index
 * @memberof MTLG#
 */
var getPlayer = function(index) {
  return players[index];
};

/**
 * This function returns all players
 * @memberof MTLG#
 */
var getPlayers = function() {
  return players;
};

/**
 * This function removes the active players from the array and thus from the framework
 * @memberof MTLG#
 */
var removeAllPlayers = function() {
  players = [];
}

/**
 * This function returns the name of a specific player
 * @param {number} index - the player's index
 * @memberof MTLG#
 */
var getPlayerName = function(index) {
  return players[index].name;
};

/**
 * This function adds the token for an existing player in the players array.
 * @param {number} index - the player's index
 * @param {object} token - the new token
 * @memberof MTLG#
 */
var setToken = function(index, token) {
  //Sanity check
  if (players.length <= index || index < 0 || !players[index]) {
    MTLG.log("core", "Unable to set token for non-existing player at index " + index);
  } else {
    players[index].token = token;
  }
}

/**
 * This function adds the token for unassigned interaction.
 * @param {object} token - the new token
 * @memberof MTLG#
 */
var setAnonToken = function(token) {
  anonToken = token;
}

/**
 * This function returns the token for an existing player in the players array.
 * @param {number} index - the player's index
 * @return {object} token - the token of the player at index or undefined if the player or token does not exist
 * @memberof MTLG#
 */
var getToken = function(index) {
  //Sanity check
  if (players.length <= index || index < 0 || !players[index]) {
    MTLG.log("core", "Unable to get token of non-existing player at index " + index);
    return undefined;
  } else {
    return players[index].token;
  }
}

/**
 * This function returns the token for unspecific interaction.
 * @return {object} token - the token
 * @memberof MTLG#
 */
var getAnonToken = function() {
  return anonToken;
}

/**
 * This functions returns the options object
 * @memberof MTLG#
 */
var getOptions = function() {
  return options;
};

/**
 * This functions returns the settings object
 * @return {object}  the stage
 * @memberof MTLG#
 */
var getSettings = function() {
  return settings;
};


/**
 * Returns the current module settings
 * @return {object}  the module settings
 * @memberof MTLG#
 */
var getModulsMTLG = function() {
  return modulsMTLG;
}

/**
 * a function to set a background image on a seperate stage.
 * @param {string} src - The path of the background image to be set relative to the img/ folder
 * @memberof MTLG#
 */
var setBackgroundImage = function(src) {
  let bgstage = backgroundStage;

  var bg = MTLG.assets.getBitmap('img/' + src);
  bgstage.addChild(bg);
  bgstage.update();
};

/**
 * a function to set a background image on a seperate stage. Stretches picture to fit stage
 * @param {string} src - The path of the background image to be set relative to the img/ folder
 * @memberof MTLG#
 */
var setBackgroundImageFill = function(src) {
  let bgstage = backgroundStage;
  var bg = MTLG.assets.getBitmap('img/' + src);

  if (MTLG.getOptions().responsive) {
    bg.scaleX = screen.width / bg.getBounds().width;
    bg.scaleY = screen.height / bg.getBounds().height;
  } else {
    bg.scaleX = MTLG.getOptions().width / bg.getBounds().width;
    bg.scaleY = MTLG.getOptions().height / bg.getBounds().height;
  }

  bgstage.addChild(bg);
  bgstage.update();
};

/**
 * a function to set a background color on a seperate stage.
 * @param {string} color - The color that will be set
 * @memberof MTLG#
 */
var setBackgroundColor = function(color) {
  let c = new createjs.Shape();
  c.graphics.beginFill(color).drawRect(0, 0, options.width, options.height);
  backgroundStage.addChild(c);
  backgroundStage.update();
};

/**
 * a function to set a createjs container as background on a seperate stage.
 * @param {container} color - The container that will be set
 * @memberof MTLG#
 */
var setBackgroundContainer = function(container) {
  backgroundStage.addChild(container);
  backgroundStage.update();
};

/**
 * deletes everything on the background stage
 * @memberof MTLG#
 */
var clearBackground = function() {
  backgroundStage.removeAllChildren();
  backgroundStage.update();
};

/**
 * returns the background stage
 * @memberof MTLG#
 */
var getBackgroundStage = function() {
  return backgroundStage;
};

/**
 * returns the container that is used to simulate the stage
 * @memberof MTLG#
 */
var getStageContainer = function() {
  return stageContainer;
};

/**
 * returns the current scaling factor
 * @memberof MTLG#
 */
var getScaleFactor = function() {
  return scaleFactor || 1;
};

// Shortend functions
var l = (key) => MTLG.lang.getString(key);
var bmp = (id) => MTLG.assets.getBitmap(id);

/////////////////////////////////////

Object.assign(exports, {
  l: l,
  bmp: bmp,
  init: mtlg_init,
  setBackgroundImage,
  setBackgroundImageFill,
  setBackgroundColor,
  clearBackground,
  getBackgroundStage,
  setBackgroundContainer,
  getScaleFactor,
  calculateCoordinates: (...args) => {
    MTLG.deprecate("MTLG.calculateCoordinates()", "code inside the game");
    return calculateCoordinates(...args);
  },
  getStageContainer,
  getStage,
  clearStage,
  loadSettings,
  getSettings,
  loadModulsMTLG,
  getModulsMTLG,
  loadOptions,
  getOptions,
  addPlayer,
  getPlayerNumber,
  getPlayer,
  getPlayers,
  updatePlayer,
  removeAllPlayers,
  getPlayerName,
  setToken,
  getToken,
  setAnonToken,
  getAnonToken,
  resize: resize_canvas,
  addGameInit,
  createAreas: (...args) => {
    MTLG.deprecate("MTLG.createAreas()", "code inside the game");
    return [];
  },
  createContainers: (...args) => {
    MTLG.deprecate("MTLG.createContainers()", "code inside the game");
    return [];
  },
  scaleUser: (...args) => {
    MTLG.deprecate("MTLG.scaleUser()", "code inside the game");
    return scaleUser(...args);
  },
  getBiggestFreeArea: (...args) => {
    MTLG.deprecate("MTLG.getBiggestFreeArea()", "code inside the game");
    return getBiggestFreeArea(...args);
  }
});
