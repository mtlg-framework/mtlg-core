/**
 * @Author: thiemo
 * @Date:   2017-10-08T18:57:38+02:00
 * @Last modified by: Vincent
 * @Last modified time: 2017-11-03T13:27:08+01:00
 */



/**
 * The asset manager module takes care of preloading and serving assets like pictures and sound.
 * @namespace assets
 * @memberof MTLG
 */
var a = {};

// LoadQueue that takes care of asset-ids, downloading and progress-listening
var q = new createjs.LoadQueue(true);
// less connections for more responsive load
q.setMaxConnections(4);
// monitor download progress of all assets
var assetProgress = {};
// callback hook when asset donwloading progresses
var progressHook = ()=>{};
// promise that fulfills as asoon as all asset-metadata is loaded and all assets are enqueued for download
var allAssetsEnqueued;
var options;

/*
 * Initializes the module, starts the preload process.
 * @param {object} opt - the options currently available to the framework
 * @param {function} callback - the callback function that is called after the module finished loading
 * @return {string}  The module handle.
 * @memberof MTLG.assets#
 */
var init = function (opt){
  q.installPlugin(createjs.Sound);
  createjs.Sound.EXTENSION_MAP["opus"] = "ogg";
  // replace and add some extensions (see https://www.createjs.com/docs/preloadjs/files/preloadjs_utils_RequestUtils.js.html)
  createjs.RequestUtils._getTypeByExtension = createjs.RequestUtils.getTypeByExtension;
  createjs.RequestUtils.getTypeByExtension = (extension) => ({
      // treat svg as image instead of svg-dom node to disable any special handling
      'svg':createjs.Types.IMAGE,
      'webm':createjs.Types.VIDEO,
      'opus':createjs.Types.SOUND
    }[(extension||'').toLowerCase()] || createjs.RequestUtils._getTypeByExtension(extension))

  // enqueue all assets for downloading
  allAssetsEnqueued = new Promise((yes,no) => {
    var i = 0;
    var fileprogressListener = q.on("fileprogress", ({item,progress}) => {
      assetProgress[item.id] = progress;
      progressHook();
    })
    var loadAssetList = path => {
      i++;
      fetch(path)
      .then(res => res.json())
      .then(json => {
        MTLG.assets.loadLater(json);
        if(--i == 0) {
          yes();
          var listener = q.on("complete", () => {
            q.off("complete", listener);
            q.off("fileprogress", fileprogressListener);
          })
        }
      }).catch(e => {no(); MTLG.warn("assets", e)});
    }
    // add all images and videos to the createjs loading queue
    loadAssetList("imageassets.json");
    // add all sounds to the createjs loading queue
    loadAssetList("soundassets.json");
    // add all fonts to the html head
    loadFontList("fontassets.json");
  })
  options = opt;
};

/**
 * Enqueues asset-urls for download. The assets will automatically start downloading as soon as all previously enqueued assets were downloaded.
 * @param {array} paths - The relative paths of the assets. An asset's path equals its id.
 * @memberof MTLG.assets#
 */
var loadLater = function(paths) {
  // add only those assets that were not added yet
  for(p of paths) {
    if(!(p in q._loadedResults)
      && !q._currentLoads.find(l => l.getItem().id == p)
      && !q._loadQueue.find(l => l.getItem().id == p))
      q._addItem(p);
  }
  // resume downloading if download already finished
  q.setPaused(false);
}

/**
 * Enqueues asset-urls for download. The assets will immediately start downloading even before any previously enqueued assets finished downloading.
 * @param {array} paths - The relative paths of the assets. An asset's path equals its id. Loading all known assets if <i>paths</i> is undefined. The paths array may contain RegExp terms like <code>&#x2F;img\&#x2F;.*&#x2F;</code>. Those are matched against all known assets.
 * @param {function} [finishCallback] - Function that is called as soon as all assets are downloaded and available via [getAsset]{@link MTLG.assets#getAsset}. Doesn't take any arguments.
 * @param {function} [progressCallback] - Function that is called as soon as the download progress of any of <i>paths</i>' assets changes. Takes one object argument with the members <i>progress</i> and <i>totalProgress</i>. Those are number values between 0.0 and 1.0 and say how much bytes of <i>paths</i> assets and all enqueued assets, respectively, are already downloaded.
 * @memberof MTLG.assets#
 */
var load = function(paths, finishCallback, progressCallback) {
  allAssetsEnqueued.then(()=>{
    var allAssets = [].concat(Object.keys(q._loadedResults), q._currentLoads.map(l=>l.getItem().id), q._loadQueue.map(l=>l.getItem().id));
    if(!paths) {
      // simply load all known assets if paths is undefined
      MTLG.logOnce("assets", "loadall", "waiting for ALL assets to load (specifying a list of assets when registering the level may reduce load time)");
      paths = allAssets;
    } else if(paths.length > 0) {
      // resolve regex wildcards:
      // 1. warn if any single expression does'nt match at least one asset
      let unusedExpression = paths.find(p => allAssets.every(p2 => (typeof p == "string") ? p != p2 : !p.test(p2)));
      if(unusedExpression)
        MTLG.logOnce("assets", `unused_${unusedExpression}`, `asset ${unusedExpression} specified but not available`);
      // 2. concat all expressions
      let escape = r => r.replace(/([.?$[\]()|^])/g, "\\$1");
      let mergedRegex = new RegExp(
        paths.map(p => (typeof p == "string") ? `(${escape(p)})` : `(${p.source})` ).join("|")
      );
      // 3. filter all available assets
      paths = allAssets.filter(a => mergedRegex.test(a));
    }

    // install download progress hook if a callback was given as argument
    if(progressCallback)
      progressHook = () => {
        // sum up download progress of assets given in 'paths' argument
        var progress = 0;
        for(p of paths)
          progress += assetProgress[p] == null ? 0.0 : assetProgress[p];
        progress /= paths.length;
        // sum up download progress of all game assets
        var totalProgress = 0;
        for(p of allAssets)
          totalProgress += assetProgress[p] == null ? 0.0 : assetProgress[p];
        totalProgress /= allAssets.length;
        progressCallback({progress,totalProgress});
      }

    // ignore already loaded assets
    var remaining = paths.filter(p => !(p in q._loadedResults));

    // LiFo-enqueue remaining
    for(p of remaining) {
      // currently downloading
      // https://www.createjs.com/docs/preloadjs/files/preloadjs_LoadQueue.js.html#l517
      if(q._currentLoads.find(l => l.getItem().id == p)) continue;
      // in load queue
      // https://www.createjs.com/docs/preloadjs/files/preloadjs_LoadQueue.js.html#l525
      var i = q._loadQueue.findIndex(l => l.getItem().id == p);
      if(i == -1) {
        // add to queue
        // https://www.createjs.com/docs/preloadjs/files/preloadjs_LoadQueue.js.html#l1165
        q._addItem(p);
        i = q._loadQueue.length - 1;
      }
      // move to head of queue
      q._loadQueue.unshift(q._loadQueue.splice(i,1)[0]);
    }
    // resume downloading if download already finished
    q.setPaused(false);

    if(remaining.length == 0) {
      // call finish callback when there are no assets remaining for download
      if(progressCallback) progressHook = ()=>{};
      if(finishCallback) setTimeout(finishCallback);
    } else {
      var listener = q.on("fileload", ({item}) => {
        // set download progress if fileprogress event is not supported by server and thus wasn't set yet
        assetProgress[item.id] = 1.0;
        progressHook();
        // remove asset from list
        remaining = remaining.filter(p => p  != item.id);
        if(remaining.length == 0) {
          q.off("fileload", listener);
          if(progressCallback) progressHook = ()=>{};
          if(finishCallback) finishCallback();
        }
      })
    }
  });
}

/**
 * Returns a downloaded asset.
 * @param {string} id The id of the object that is requested. It equals the path to the requested resource.
 * @return {object} The asset that was requested or <i>null</i>, if it wasn't found or not loaded yet. The return type depends on the type of asset.
 * @memberof MTLG.assets#
 */
var getAsset = function(id){
  var instance = q.getResult(id);
  if(!instance)
    MTLG.warnOnce("assets", `notfound_${id}`, "Unable to provide preload resource: " + id);
  else
    instance.assetId = id;

  return instance;
}

/**
 * Returns a createjs bitmap of the requested picture/movie.
 * @param {string} id - path to the picture
 * @return {object} a bitmap of the requested image/movie or the empty bitmap if the requested image/movie does not exist
 * @memberof MTLG.assets#
 */
var getBitmap = (id) => new createjs.Bitmap( MTLG.assets.getAsset(id) );

/**
 * Returns a cached bitmap in a specific scale.
 * The scale is relative to the options.width and depends on the image width.
 * @param {string} id - path to image
 * @param {number} scale - new scale
 * @return {object} - bitmap of scaled image
 * @memberof MTLG.assets#
 */
var getBitmapScaled = function(id,scale){
  var instance = MTLG.assets.getBitmap(id);
  if(!instance.image){
    MTLG.log("assets", "Unable to scale bitmap for image: " + id);
    return instance;
  }
  instance.scaleX = instance.scaleY = options.width * scale / instance.image.width;
  return instance;
}

/**
 * Returns a cached bitmap in a specific scale.
 * The scale factor is in absolute values relative to the original.
 * Caches the bitmap.
 * @param {string} id - path to image
 * @param {number} scaleX - value of scale on x axis
 * @param {number} scaleY - value of scale on y axis
 * @return {object} The scaled and cached bitmap
 * @memberof MTLG.assets#
 */
var getBitmapAbsoluteScale = function(id,scaleX,scaleY){
  var instance = MTLG.assets.getBitmap(id);
  instance.scaleX = scaleX;
  instance.scaleY = scaleY;
  return instance;
}


/**
 * Returns the sound instance of the requested sound file.
 * Returns the sound instance of a dummy if the file does not exist or ist not playable.
 * @param {string} id - path to sound relative to sounds/ folder
 * @return {object} the sound instance of the requested file or a dummy if the instance could not be created.
 * @memberof MTLG.assets#
 */
var getSoundInstance = function(id){
  if(q.getResult(`sounds/${id}.opus`))
    return createjs.Sound.createInstance(`sounds/${id}.opus`);
  else if(q.getResult(`sounds/${id}.m4a`))
    return createjs.Sound.createInstance(`sounds/${id}.m4a`);
  else if(q.getResult(`sounds/${id}.mp3`))
    return createjs.Sound.createInstance(`sounds/${id}.mp3`);
  else {
    MTLG.warnOnce("assets", `notfound_${id}`, "Unable to provide sound: " + id);
    return createjs.DefaultSoundInstance;
  }
}

/**
 * Plays the requested sound and returns the sound instance.
 * @param {string} id - the path to the sound file relative to sounds/
 * @param {object} properties - Optional Play Properties Include:
 *   <ul>
 *   <li><strong>interrupt</strong> - How to interrupt any currently playing instances of audio with the same source, if the maximum number of instances of the sound are already playing. Values are defined as INTERRUPT_TYPE constants on the Sound class, with the default defined by defaultInterruptBehavior.</li>
 *   <li><strong>delay</strong> - The amount of time to delay the start of audio playback, in milliseconds.</li>
 *   <li><strong>offset</strong> - The offset from the start of the audio to begin playback, in milliseconds.</li>
 *   <li><strong>loop</strong> - How many times the audio loops when it reaches the end of playback. The default is 0 (no loops), and -1 can be used for infinite playback.</li>
 *   <li><strong>volume</strong> - The volume of the sound, between 0 and 1. Note that the master volume is applied against the individual volume.</li>
 *   <li><strong>pan</strong> - The left-right pan of the sound (if supported), between -1 (left) and 1 (right).</li>
 *   <li><strong>startTime</strong> - To create an audio sprite (with duration), the initial offset to start playback and loop from, in milliseconds.</li>
 *   <li><strong>duration</strong> - To create an audio sprite (with startTime), the amount of time to play the clip for, in milliseconds.</li>
 *   </ul>
 * @memberof MTLG.assets#
 */
var playSound = function(id, properties){
  var instance = MTLG.assets.getSoundInstance(id).play(properties);
  if(instance.playState == "playFailed"){ // This happens if the sound file does not exist
    MTLG.log("assets", "Trying to play non existing sound instance: " + id);
    setTimeout(function(){ // Set timeout so that event "on" set after play() can be triggered
      instance.dispatchEvent("complete");
    });
  }
  return instance;
}

/**
 * Returns the sound instance of the sound file id of the current language, see MTLG.lang for more information on language settings
 * @param {string} id - the sound file to be played missing the final "_lang" e.g. "someSound_en.mp3" would not include the "_en" and become "someSound"
 * @return {object} the created sound instance of a dummy if the file could not be played
 * @memberof MTLG.assets#
 */
var getLangSoundInstance = function(id){
  return MTLG.assets.getSoundInstance(id + "_" + MTLG.lang.getLanguage());
}

/**
 * Plays a language specific sound and returns the sound instance
 * @param {string} id - the sound file to be played missing the final "_lang" e.g. "someSound_en.mp3" would not include the "_en" and become "someSound"
 * @param {object} properties - the properties passed to the instance when it is played. See createjs docs for more information on available options.
 * @return {object} the created sound instance of a dummy if the file could not be played
 * @memberof MTLG.assets#
 */
var playLangSound = function(id, properties){
  return MTLG.assets.playSound(id + "_" + MTLG.lang.getLanguage(), properties);
}

/* Background Music:
 * Handle Background Music seperately
 * To make it easier to keep track of the background music instance
 */

var backgroundMusicInstance;
/**
 * plays the specified sound file as background music, i.e. it loops
 * @param {string} id - the path to the sound file
 * @memberof MTLG.assets#
 */
var playBackgroundMusic = function(id){
  if(backgroundMusicInstance){
    MTLG.assets.stopBackgroundMusic();
  }
  backgroundMusicInstance = MTLG.assets.playSound(id,{loop : -1});
}

/**
 * Stops the current background music, i.e. pauses and resets time
 * @memberof MTLG.assets#
 */
//Stop registered background music
var stopBackgroundMusic = function(){
  if(backgroundMusicInstance){
    backgroundMusicInstance.stop();
  }
}

/**
 * Pause registered background music
 * @memberof MTLG.assets#
 */
var pauseBackgroundMusic = function(){
  if(backgroundMusicInstance){
    backgroundMusicInstance.paused = true;
  }
}

/**
 * Resume registered background music
 * Can resume paused and stopped instances
 * @memberof MTLG.assets#
 */
var resumeBackgroundMusic = function(){
  if(backgroundMusicInstance){
    backgroundMusicInstance.paused = false;
  }
  //Handle stopped instances
  if(backgroundMusicInstance.playState === 'playFinished'){
    backgroundMusicInstance.play({loop : -1});
  }
}

/**
 * Sets the volume of the background music
 * @param {number} newVol - the new volume, should be a number in [0,1] where 0 is muted and 1 is normal volume.
 * @memberof MTLG.assets#
 */
var setBackgroundMusicVolume = function(newVol){
  if(backgroundMusicInstance){
    backgroundMusicInstance.volume = newVol;
  }
}

var loadFontList = function(path){
  //loads JSON
  fetch(path)
  .then(res => res.json())
  .then(json => {
    //generates CSS for all fonts
    var css = '';
    for (source of json){
      var [fontName , ext] = source.split('\\').pop().split('/').pop().split('.');
      if(ext != 'txt') css = css + '\n' + `@font-face {font-family: ${fontName}; src: url(${source}); }`;
    }
    //adds CSS to html
    var head = document.getElementsByTagName('head')[0];
    var s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    s.appendChild(document.createTextNode(css));
    head.appendChild(s);
  })
}

MTLG.assets = {
  loadLater,
  load,
  getAsset,
  getBitmap,
  getBitmapScaled,
  getBitmapAbsoluteScale,
  getSoundInstance,
  playSound,
  getLangSoundInstance,
  playLangSound,
  playBackgroundMusic,
  stopBackgroundMusic,
  pauseBackgroundMusic,
  resumeBackgroundMusic,
  setBackgroundMusicVolume
};
MTLG.addModule(init);
