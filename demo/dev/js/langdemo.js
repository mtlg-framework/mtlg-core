
function langLevel() {
  // Access the root container to which all UI elements will be added.
  var stage = MTLG.getStageContainer();

  var statusLabel = new createjs.Text("", "80px Arial").set({x:860, color:'yellow'});
  var updateLabel = function() {
    statusLabel.text = `
getLanguage: ${MTLG.lang.getLanguage()}
getString: ${MTLG.lang.getString('_car')}
`
  }
  updateLabel();

  var button1 = createButton("define").set({x:20, y:100});
  button1.on("click", () => {
    MTLG.lang.define({'en':{'_car':'car'},'de':{'_car':'Auto'}});
    updateLabel();
  });

  var button2 = createButton("setLanguage").set({x:20, y:180});
  button2.on("click", () => {
    MTLG.lang.setLanguage(MTLG.lang.getLanguage() == 'en' ? 'de' : 'en');
    updateLabel();
  });

  stage.addChild(button1, button2, statusLabel);
}
