
function assetLevel() {
  // Access the root container to which all UI elements will be added.
  var stage = MTLG.getStageContainer();

  var button1 = createButton("getAsset (see console)").set({x:20, y:100});
  button1.on("click", () => {
    var img = MTLG.assets.getAsset("img/rorschach.png");
    var sound = MTLG.assets.getAsset("sounds/demo.m4a");
    console.log(img, sound);
  });

  var button2 = createButton("getBitmap").set({x:20, y:180});
  button2.on("click", () => {
    var bmp = MTLG.assets.getBitmap("img/coin.svg");
    bmp.set({x:300, y:50});
    stage.addChild( bmp );
  });

  var button3 = createButton("getBitmapAbsoluteScale").set({x:20, y:260});
  button3.on("click", () => {
    var bmp = MTLG.assets.getBitmapAbsoluteScale("img/coin.svg", .5, .5);
    bmp.set({x:300, y:50});
    stage.addChild( bmp );
  });

  var button4 = createButton("getBitmapScaled").set({x:20, y:340});
  button4.on("click", () => {
    var bmp = MTLG.assets.getBitmapScaled("img/coin.svg", 1.0);
    stage.addChild( bmp );
  });

  var button5 = createButton("getLangSoundInstance").set({x:20, y:420});
  button5.on("click", () => {
    MTLG.lang.setLanguage("en");
    var sound = MTLG.assets.getLangSoundInstance("good");
    sound.play();
    setTimeout(() => {
      MTLG.lang.setLanguage("de");
      var sound = MTLG.assets.getLangSoundInstance("good");
      sound.play();
    }, 500);
  });

  var button6 = createButton("getSoundInstance").set({x:20, y:500});
  button6.on("click", () => {
    var sound = MTLG.assets.getSoundInstance("demo");
    sound.play();
  });

  var button7 = createButton("playLangSound").set({x:20, y:580});
  button7.on("click", () => {
    MTLG.lang.setLanguage("en");
    MTLG.assets.playLangSound("good");
    setTimeout(() => {
      MTLG.lang.setLanguage("de");
      MTLG.assets.playLangSound("good");
    }, 500);

  });

  var button8 = createButton("playSound").set({x:20, y:660});
  button8.on("click", () => {
    MTLG.assets.playSound("demo");
  });

  var button9 = createButton("playBackgroundMusic").set({x:860, y:100});
  button9.on("click", () => {
    MTLG.assets.playBackgroundMusic("background");
  });

  var button10 = createButton("pauseBackgroundMusic").set({x:860, y:180});
  button10.on("click", () => {
    MTLG.assets.pauseBackgroundMusic();
  });

  var button11 = createButton("resumeBackgroundMusic").set({x:860, y:260});
  button11.on("click", () => {
    MTLG.assets.resumeBackgroundMusic();
  });

  var button12 = createButton("setBackgroundMusicVolume").set({x:860, y:340});
  button12.on("click", () => {
    MTLG.assets.setBackgroundMusicVolume(0.5);
  });

  var button13 = createButton("stopBackgroundMusic").set({x:860, y:420});
  button13.on("click", () => {
    MTLG.assets.stopBackgroundMusic();
  });

  stage.addChild(button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13);
}
